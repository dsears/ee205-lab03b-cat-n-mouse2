#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo OK cat, I\'m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

read userInput

#echo The number you chose was $userInput

if [ $userInput -lt 1 ]
  then
  echo The number you enter must be greater than 0 and less than $THE_MAX_VALUE
fi  
  #echo The number you chose is greater than 1
if [ $userInput -gt $THE_MAX_VALUE ]
  then
  echo The number you enter must be less than $THE_MAX_VALUE
fi
while [ $userInput -ne $THE_NUMBER_IM_THINKING_OF ]
do
   #read userInput
   if [ $userInput -lt $THE_NUMBER_IM_THINKING_OF ]
   then
   echo No, cat. The number I am thinking of is greater than $userInput
   fi
   if [ $userInput -gt $THE_NUMBER_IM_THINKING_OF ]
   then
   echo No, cat. The number I am thinking of is less than $userInput
   fi
   read userInput
   if [ $userInput -lt 1 ]
  then
  echo The number you enter must be greater than 0 and less than $THE_MAX_VALUE
fi 
if [ $userInput -gt $THE_MAX_VALUE ]
  then
  echo The number you enter must be less than $THE_MAX_VALUE
fi
done
echo You got me.
echo  " " \/\\\_\/\\
echo " "\( \o\.\o \)
echo  " " \> \^ \<
