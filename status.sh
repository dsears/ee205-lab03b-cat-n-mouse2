#!/bin/bash

echo "I am"

echo $(whoami)

echo "The current working directory is"

echo $(pwd)

echo "The system I am on is"

echo $(uname -n)

echo "The linux version is"

echo $(uname -r)

echo "The linux distribution is"

echo $(cat /etc/fedora-release)

echo "The system has been up for"

echo $(uptime)

echo "The amount of disk space I am using in KB is"

echo $(du -k ~ | awk 'END{print}')
